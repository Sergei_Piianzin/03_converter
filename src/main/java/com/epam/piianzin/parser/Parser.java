package com.epam.piianzin.parser;

@FunctionalInterface
public interface Parser<T> {
  public T parse();
}
