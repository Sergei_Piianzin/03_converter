package com.epam.piianzin.parser;

import java.io.IOException;
import java.io.InputStream;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonParser<T> implements Parser<T> {
  private String pathToJson;
  private Class<T> clazz;

  public JsonParser(String pathToJson, Class<T> t) {
    this.pathToJson = pathToJson;
    clazz = t;
  }

  @Override
  public T parse() {
    ObjectMapper mapper = new ObjectMapper();
    mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
    InputStream is = this.getClass().getResourceAsStream(pathToJson);
    try {
      return (T) mapper.readValue(is, clazz);
    } catch (JsonParseException e) {
      e.printStackTrace();
    } catch (JsonMappingException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
    return null;
  }
}
