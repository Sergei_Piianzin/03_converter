package com.epam.piianzin.parser;

import java.io.InputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

public class XmlParser<T> implements Parser<T> {
  String pathToXml;
  Class<T> clazz;

  public XmlParser(String pathToXml, Class<T> t) {
    this.pathToXml = pathToXml;
    this.clazz = t;
  }

  @Override
  public T parse() {
    JAXBContext jaxbContext;
    InputStream is = this.getClass().getResourceAsStream(pathToXml);
    try {
      jaxbContext = JAXBContext.newInstance(clazz);
      Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
      return (T) unmarshaller.unmarshal(is);
    } catch (JAXBException e) {
      e.printStackTrace();
    }
    return null;
  }
}
