package com.epam.piianzin.parser;

import java.io.InputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.XMLEvent;

import com.epam.piianzin.pojo.Item;

public class XmlItemParser {

  private String pathToXml;

  public XmlItemParser(String pathToXml) {
    this.pathToXml = pathToXml;
  }

  public void parse() {
    XMLInputFactory xif = XMLInputFactory.newFactory();
    InputStream xmlStream = this.getClass().getResourceAsStream(pathToXml);
    XMLEventReader xer;

    try {
      xer = xif.createXMLEventReader(xmlStream);
      JAXBContext jc = JAXBContext.newInstance(Item.class);
      Unmarshaller unmarshaller = jc.createUnmarshaller();
      while (xer.hasNext()) {
        XMLEvent e = xer.peek();
        if (e.isStartElement() && e.asStartElement().getName().getLocalPart().equals("item")) {
          JAXBElement<Item> jb = unmarshaller.unmarshal(xer, Item.class);
          System.out.println(jb.getValue());
        } else {
          xer.nextEvent();
        }
      }
      xer.close();
      System.out.println("Parse xml and write items. Done.\n");
    } catch (XMLStreamException | JAXBException e) {
      e.printStackTrace();
    }

  }

}
