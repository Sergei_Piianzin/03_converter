package com.epam.piianzin.writer;

import java.io.File;
import java.io.IOException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

public class JsonWriter<T> implements Writer<T> {
  private String path;

  public JsonWriter(String path) {
    this.path = path;
  }

  @Override
  public void write(T t) {
    ObjectMapper mapper = new ObjectMapper();
    mapper.enable(SerializationFeature.INDENT_OUTPUT);
    try {
      mapper.writeValue(new File(path), t);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
