package com.epam.piianzin.writer;

public interface Writer<T> {
  void write(T t);
}
