package com.epam.piianzin.writer;

import java.io.File;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

public class XmlWriter<T> implements Writer<T> {
  private String pathToXml;
  private Class<T> clazz;

  public XmlWriter(String path, Class<T> clazz) {
    this.pathToXml = path;
    this.clazz = clazz;
  }

  @Override
  public void write(T t) {
    JAXBContext jaxbContext;
    try {
      jaxbContext = JAXBContext.newInstance(clazz);
      Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
      jaxbMarshaller.setProperty("jaxb.formatted.output", true);
      jaxbMarshaller.marshal(t, new File(pathToXml));
    } catch (JAXBException e) {
      e.printStackTrace();
    }

  }
}
