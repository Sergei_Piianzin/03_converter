package com.epam.piianzin.pojo;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonProperty;

@XmlRootElement
public class Categories {
  @XmlElement(name = "category")
  @JsonProperty(value = "category")
  public List<Category> categories;
}
