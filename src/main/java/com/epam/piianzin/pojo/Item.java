package com.epam.piianzin.pojo;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import javax.xml.bind.annotation.*;

@XmlRootElement
public class Item {
  @XmlElement
  public String producer;
  @XmlElement
  public String model;
  private LocalDate date;
  @XmlElement
  public String color;
  @XmlElement
  public double price;
  @XmlElement
  public int count;

  @XmlAttribute(required = true)
  public int id;

  public String getDate() {
    return date.format(DateTimeFormatter.ofPattern("dd-MM-yyyy"));
  }

  @XmlElement(name = "data")
  public void setDate(String date) {
    this.date = LocalDate.parse(date, DateTimeFormatter.ofPattern("dd-MM-yyyy"));
  }

  public Item() {

  }

  public Item(String madeBy, String model, LocalDate date, String color, double price, int count) {
    this.producer = madeBy;
    this.model = model;
    this.date = date;
    this.color = color;
    this.price = price;
    this.count = count;
  }

  @Override
  public String toString() {
    StringBuilder result = new StringBuilder();
    result.append("id: ").append(id).append("\n").append("Producer: ").append(producer).append("\n")
        .append("Model: ").append(model).append("\n").append("Date: ")
        .append(date.format(DateTimeFormatter.ofPattern("dd-MM-yyyy"))).append("\n")
        .append("Color: ").append(color).append("\n").append("Price: ").append(price).append("\n")
        .append("Count: ").append(count).append("\n\n");
    return result.toString();
  }
}
