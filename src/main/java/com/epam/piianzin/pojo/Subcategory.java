package com.epam.piianzin.pojo;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.*;

@XmlRootElement
public class Subcategory {
  private List<Item> items;
  @XmlAttribute
  public String title;

  public List<Item> getItems() {
    return items;
  }

  @XmlElement(name = "item")
  public void setItems(List<Item> items) {
    this.items = items;
  }

  public Subcategory() {

  }

  public Subcategory(ArrayList<Item> items, String title) {
    this.items = items;
    this.title = title;
  }

  @Override
  public String toString() {
    StringBuilder result = new StringBuilder();
    result.append("Subcategory title: ").append(title).append("\n");
    for (Item i : items) {
      result.append(i);
    }
    return result.toString();
  }

}
