package com.epam.piianzin.pojo;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.*;

@XmlRootElement
public class Category {
  private List<Subcategory> subcategories;
  
  @XmlAttribute
  public String title;

  public Category() {

  }

  public Category(ArrayList<Subcategory> subcategories,  String title) {
    this.subcategories = subcategories;
    this.title = title;
  }

  public List<Subcategory> getSubcategories() {
    return subcategories;
  }

  @XmlElement(name="subcategory")
  public void setSubcategories(List<Subcategory> subcategories) {
    this.subcategories = subcategories;
  }
  
  @Override
  public String toString() {
    StringBuilder result = new StringBuilder();
    result.append("Category title: ").append(title).append("\n");
    for(Subcategory sub: subcategories) {
      result.append(sub);
    }
    return result.toString();
  }


}
