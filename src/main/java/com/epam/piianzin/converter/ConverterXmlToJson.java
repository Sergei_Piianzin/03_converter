package com.epam.piianzin.converter;

import com.epam.piianzin.parser.XmlParser;
import com.epam.piianzin.writer.JsonWriter;

public class ConverterXmlToJson<T> implements Converter<XmlParser<T>, JsonWriter<T>> {
  XmlParser<T> xmlParser;
  JsonWriter<T> jsonWriter;

  public ConverterXmlToJson(String pathXml, String pathJson, Class<T> t) {
    xmlParser = new XmlParser<>(pathXml, t);
    jsonWriter = new JsonWriter<>(pathJson);
  }

  @Override
  public void convert() {
    T t = xmlParser.parse();
    jsonWriter.write(t);
    System.out.println("Convert xml to json. Done.\n");
  }
}
