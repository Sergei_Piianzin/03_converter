package com.epam.piianzin.converter;

import com.epam.piianzin.parser.JsonParser;
import com.epam.piianzin.writer.XmlWriter;

public class ConverterJsonToXml<T> implements Converter<JsonParser<T>, XmlWriter<T>> {
  JsonParser<T> jsonParser;
  XmlWriter<T> xmlWriter;

  public ConverterJsonToXml(String jsonPath, String xmlPath, Class<T> t) {
    jsonParser = new JsonParser<>(jsonPath, t);
    xmlWriter = new XmlWriter<>(xmlPath, t);
  }

  @Override
  public void convert() {
    T t = jsonParser.parse();
    xmlWriter.write(t);
    System.out.println("Convert json to xml. Done.\n");
  }
}
