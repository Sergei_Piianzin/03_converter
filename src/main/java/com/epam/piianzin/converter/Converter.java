package com.epam.piianzin.converter;

@FunctionalInterface
public interface Converter<T, Q> {
  void convert();
}
