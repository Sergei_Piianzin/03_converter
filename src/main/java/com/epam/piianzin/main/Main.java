package com.epam.piianzin.main;

import java.util.Scanner;

import com.epam.piianzin.converter.ConverterJsonToXml;
import com.epam.piianzin.converter.ConverterXmlToJson;
import com.epam.piianzin.parser.XmlItemParser;
import com.epam.piianzin.pojo.Categories;

public class Main {
  public static void main(String[] args) {
    Main app = new Main();
	//hello
    app.run();
    System.out.println("Good bye.");
  }

  public void run() {
    String root = "/resources/";
    String xml = root + "shop.xml";
    String schema = root + "schema.xml";
    String json = root + "shop.json";

    boolean run = true;
    while (run) {
      StringBuilder inputMessage = new StringBuilder();
      inputMessage.append("Please input one of these commands:\n").append("-s show xml items\n")
          .append("-x xml to json\n").append("-j json to xml\n").append("-v validate xml\n")
          .append("-q quit\n");
      System.out.println(inputMessage);

      Scanner sc = new Scanner(System.in);
      String input = sc.next();
      switch (input) {
        case "-s":
          new XmlItemParser(xml).parse();
          break;
        case "-x":
          new ConverterXmlToJson<>(xml, "result.json", Categories.class).convert();
          break;
        case "-j":
          new ConverterJsonToXml<>(json, "result.xml", Categories.class).convert();
          break;
        case "-v":
          Validator.validateXml(xml, schema);
          break;
        case "-q":
          run = false;
          sc.close();
          break;
        default:
          System.out.println("Wrong input.");
          break;
      }
    }
  }
}
