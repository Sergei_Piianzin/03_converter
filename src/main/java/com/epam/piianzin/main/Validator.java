package com.epam.piianzin.main;

import java.io.IOException;
import java.io.InputStream;

import javax.xml.XMLConstants;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.xml.sax.SAXException;

public class Validator {
  public static void validateXml(String xml, String schemaPath) {
    SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
    Schema schema;

    InputStream xmlStream = Main.class.getResourceAsStream(xml);
    InputStream schemaStream = Main.class.getResourceAsStream(xml);
    try {
      schema = schemaFactory.newSchema(new StreamSource(schemaStream));
      javax.xml.validation.Validator validator = schema.newValidator();
      validator.validate(new StreamSource(xmlStream));
      System.out.println(xml + " is valide (" + schemaPath + ")");
    } catch (SAXException | IOException e) {
      e.printStackTrace();
    }
  }
}
